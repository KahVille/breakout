breakout clone
this is a school project. Breakout clone made with SFML2

BUILDING instructions
Windows 10 64-bit mingw and cmake
1. Download SFML-mingw 64-bit version (SEH) and correct mingw version, MINGW64-bit (SEH)
2. Download and install CMake 64-bit version, minimum 3.5
3. Download breakout sources
4. Unpack SFML and mingw64 to location of your choosing.
5. Unpack Breakout sources to location of your choosing.
6. make sure that you have cmake/bin and mingw64/bin in your enviroment variables
7. navigate to root of unpacked breakout sources
8. make build directory "folder" and change current path to it
9. Open terminal here
10. run command: cmake .. -G "MinGW Makefiles" -DSFML_ROOT:PATH="C:\path\to\your\sfml\folder"
11. if no errors appear run command: mingw32-make
12. if no errors appear copy all .dll files from "sfmldirectory/bin"  to folder where breakout.exe file is.
13. run breakout.exe

Linux
Depencies: CMake 3.5, make and SFML

1. install depencies
Ubuntu:
install cmake and make
apt-get install cmake make
install SFML
apt-get install libsfml-dev

2. Download breakout clone sources and unpack them to location of your choosing.
3. make sure that you have cmake and make in your enviroment variables //for linux this is usually the case
4. navigate to root of unpacked breakout sources
5. make build directory and change current path to it
6. Open terminal inside the build directory
7. run command: cmake ..
8. if no errors appear run command: make all
9. if no errors appear run breakout clone executable ./breakout

INSTALL/RUN instructions

Windows 10 64-bit
1. Dowload Release version of breakout and unpack it. Folder includes all .dll files needed and an executable.
2. Run breakout.exe from the unpacked folder

Linux
1. Dowload  binary and sources zip and run ./breakout

Keybinds
1. F toggle fullscreen
2. Move with mouse
3. unlock the game with Mouse1 button
