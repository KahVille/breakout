#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Tile.h"
class Level {

private:
     std::vector <Tile*> tilemap;
     int offsetx = 63;
     int offsety = 32;

public:
      Level ();
      ~Level ();

void updateTiles();
std::vector<Tile*> getTilemap();
void addTiles(std::vector<int> leveldata);
      void removetile(Tile* brik);
      void cleanAll();
      void print();
      bool iscomplete();

};
