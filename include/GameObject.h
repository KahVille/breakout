#pragma once

#include <iostream>
#include <SFML/Graphics.hpp>

class GameObject {
protected:
    sf::Sprite sprite;        //sprite that is shown on screen
    sf::Texture texture;    //loaded image file
    sf::Vector2f position; //objects position draw also by this

public:
    GameObject();
    ~GameObject();

    void loadTexture(std::string filename);
    sf::Sprite getSprite();

    sf::Vector2f getPosition();
    void setPosition(sf::Vector2f pos);

    void setSpriteRot(int rot);
    void setSprite(std::string filename);

};
