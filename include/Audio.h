#pragma once

#include <iostream>
#include <SFML/Audio.hpp>

class Audio {
private:
	 sf::SoundBuffer buffer;
	 sf::Sound sound;
public:
	 Audio(std::string filename);

	~Audio ();

	void playSound();
};
