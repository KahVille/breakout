#pragma once
#include <iostream>
#include "GameObject.h"

class Paddle : public GameObject {

public:

    Paddle ();
    ~Paddle();

    float getWidth();
    float getHeight();

    void setWidth(float width);
    void setHeight(float height);

    sf::Sprite getSkin();

    void update(float mousex);

private:

    float width;
    float height;

};
