#pragma once
#include "Tile.h"

class LevelLoader {
private:
  int currentLevel = 0;
  int nextlevel = 0;
  Tile * tile;
public:
  LevelLoader () {
  
  }
  ~LevelLoader ();

  GenerateLevel();
  LoadLevel(int level);
};
