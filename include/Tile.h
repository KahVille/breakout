#pragma once
#include <iostream>
#include "GameObject.h"

class Tile : public GameObject {

public:

    Tile ();
    ~Tile();

    float getWidth();
    float getHeight();

    void setWidth(float width);
    void setHeight(float height);

    sf::Sprite getSkin();

    void update();
    void Damaged();
    int getHP() {return this->health;}
    
    void setHP(int hp) {
        this->health = hp;
    }
    void updateTexture(int x, int y);

private:

    float width;
    float height;

    int health = 0;

};
