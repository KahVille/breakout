#pragma once

#include <iostream>
#include <fstream>
#include <vector>

//sfml headers
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

//game headers
#include "Ball.h"
#include "GameArea.h"
#include "Renderer.h"
#include "Paddle.h"
#include "Wall.h"
#include "Audio.h"
#include "Tile.h"
#include "Level.h"

class Game {
private:
  GameArea* pgameArea;
  Ball* pball;
  Renderer* renderer;
  Paddle* ppaddle;
  Wall* topwall;
  Wall* rightwall;
  Wall* leftwall;
  Audio* bounceSound;
  Audio* deathSound;
  Level* level;
  sf::Music mainmusic;

  // open a file in write mode.
  std::ifstream infilestream;
  std::string line;

//number of bricks and emptys
  int numberofemptys=0;
  int numberofbriks=0;

//follow the current level
  int currentlevel = 0;
  int nextlevel = 0;

//store all the levels to this vector
  std::vector<int> levelsdata;

//collisions for objects
  void CheckCollisions();
   void tileCheck();
  void wallsCheck();
     void paddleCheck();

//advanve to the next level is possible
  void advanceLevel();
 
public:
  Game(sf::RenderWindow* window);
  ~Game();

//read the levels data from text file
  void ReadLevels();

//create level from levels data
  void createLevel(int level);


  enum GameStates {Playing, Ended, Paused};
  GameStates gameState;

//game functions
  void Update(float paddlex);
  void Render(sf::RenderWindow* pWindow);
  void HandleInput();
  void reset();


};
