#pragma once

#include <iostream>
#include "GameObject.h"

class Ball : public GameObject {
private:

    sf::Vector2f velocity;

    float radius;
    float centerpoint;
    

public:
    Ball(float radius);
    ~Ball();
    float getRadius();
    void setRadius(float radius);
    float getCenterPoint();

    sf::Vector2f getVelocity();
    void setVelocity(sf::Vector2f vel);

    void bounceOff(sf::Vector2f n);
    void bounceVertically();
    void bounceHorizontally();
    sf::Sprite getSkin();

    void update();
};
