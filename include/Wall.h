#pragma once

#include <iostream>
#include "GameObject.h"

class Wall : public GameObject {
private:
    float width;
    float height;
	float thickness;
public:
	Wall(float width, float height);

	~Wall();
	sf::Sprite getSkin();

	float getWidth();
	float getHeight();
	float getThickness();

	void setWidth(float width);
	void setHeight(float height);
	void setThickness(float thickness);

	void update();
};
