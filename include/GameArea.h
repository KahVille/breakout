#pragma once

#include <iostream>

class GameArea {
private:

	float width;
	float height;

	float wallthickness;

public:

	GameArea();
	GameArea(int width, int height);
	~GameArea();

	float GetWidth();
	void SetWidth(float width);

	float GetHeight();
	void SetHeight(float height);

	float getWallThickness();
	void setWallThickness(float thickness);
};
