#include "Audio.h"


Audio::Audio (std::string filename)
{
	if (!buffer.loadFromFile(filename))
    {
        //std::cout << "sound could not be loaded" << '\n';
    }
    else
    { 
        sound.setBuffer(buffer);
      //std::cout << "sound loaded" << '\n';
    }
}

Audio::~Audio ()
{

}
void Audio::playSound() {
	sound.play();
}
