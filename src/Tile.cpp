#include "Tile.h"

Tile::Tile() : GameObject()
{
    this->width = 62.0;
    this->height = 30.0;
    //GameObject::sprite.setColor(sf::Color::Red);
    //std::cout << "tile created" << '\n';
    health = 1;
    GameObject::setSprite("../textures/BasicArkanoidPack.png");
    GameObject::sprite.setTextureRect(sf::IntRect(0,0, this->width, this->height));
    GameObject::setPosition(sf::Vector2f(300.0f,300.0f));
    
}

Tile::~Tile()
{
  this->width = 0;
  this->height = 0;
}

float Tile::getWidth()
{
  return this->width;
}

float Tile::getHeight()
{
  this->height;
}

void Tile::setWidth(float width)
{
  this->width = width;
}
void Tile::setHeight(float height)
{
  this->height = height;
}

sf::Sprite Tile::getSkin()
{
  return GameObject::sprite;
}

void Tile::update()
{

}

void Tile::Damaged() {
  health--;
    if(this->health <= 0) {
    this->health = 0;
    }

  if(this->health == 1) {
  updateTexture(144,0);
  }
  if(this->health == 2) {
  updateTexture(0,0);
  }
  if(this->health == 3) {
  updateTexture(72,0);
  }
   //std::cout << "my health is " << this->health;
 
}

void Tile::updateTexture(int x, int y) {
      GameObject::sprite.setTextureRect(sf::IntRect(x,y, this->width, this->height));
}
