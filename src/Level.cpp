#include "Level.h"

Level::Level()
{
        //std::cout << "level created" << '\n';
}

Level::~Level()
{
    //std::cout << "level Destroyed" << '\n';
}

std::vector<Tile*> Level::getTilemap() { return tilemap; }

void Level::print() 
{
    for(Tile* n : tilemap) {
            std::cout << n->getPosition().x << '\n';
    }
}

bool Level::iscomplete()
{
    if(tilemap.empty()) return true;

    return false;
}

void Level::addTiles(std::vector<int> leveldata) 
{
    int levelwidth = 0;

    int tileoffsetx = 0;
    int tileoffsety = 0;

        for (auto it :leveldata)
        {
          levelwidth++;

          if(it == 0)
        {
            //free powerups
        }
            tileoffsetx = tileoffsetx + offsetx;
         
            Tile *tmp = new Tile();
            tmp->setPosition(sf::Vector2f((tmp->getPosition().x + tileoffsetx) ,(tmp->getPosition().y + tileoffsety) ));

            if(it == 1)
            {
                tmp->updateTexture(144,0); //green
                tmp->setHP(1);
                tilemap.push_back(tmp);

            }
            if(it == 2)
            {

                tmp->updateTexture(0,0); // yellow
                tmp->setHP(2);
                tilemap.push_back(tmp);
            }
            if(it == 3)
            {
                tmp->updateTexture(72,0); //red
                tmp->setHP(3);
                tilemap.push_back(tmp);
            }
        //reset rowoffset when end of brickrow
          if(levelwidth >2)
          {
            levelwidth = 0;
            tileoffsetx = 0;
            tileoffsety = tileoffsety + offsety;
        }
      }
}


void Level::removetile(Tile* brik)
{
// removes all elements with the value of brik //all briks are unique gameobjects so there is no conflict
this->tilemap.erase( std::remove( this->tilemap.begin(), this->tilemap.end(), brik ), this->tilemap.end() );
delete brik;
}

void Level::cleanAll()
{
// removes all elements from tilemapvector
     tilemap.erase( tilemap.begin(), this->tilemap.end());
}

void Level::updateTiles() 
{
    for (auto it :tilemap) 
    {
        if(it->getHP() <=0) 
        {
            removetile(it);
        }
    }
}