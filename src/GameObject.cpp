#include "GameObject.h"

GameObject::GameObject ()
{
 this-> position = sf::Vector2f(0.0f, 0.0f);
}

GameObject::~GameObject ()
{
    this-> position = sf::Vector2f(0.0f, 0.0f);
}

void GameObject::loadTexture(std::string filename)
{
    if (!texture.loadFromFile(filename))
    {
        //std::cout << "texture could not be loaded" << '\n';
    }
    else {  
        //std::cout << "texture loaded" << '\n';
  }

}

sf::Sprite GameObject::getSprite() { return this->sprite; }

sf::Vector2f GameObject::getPosition() {return this->position; }

void GameObject::setPosition(sf::Vector2f pos)
{
    this->position = pos;
    this->sprite.setPosition(pos);
}

void GameObject::setSpriteRot(int rot) 
{
    sprite.setRotation(rot);
}


void GameObject::setSprite(std::string filename) 
{
    if (!texture.loadFromFile(filename))
    {
        //std::cout << "texture could not be loaded" << '\n';
    }
    else
    { 
        sprite.setTexture(texture);
      //std::cout << "texture loaded" << '\n';
    }
 
    sprite.setPosition(this->position); // absolute position
    texture.setRepeated(true);
}
