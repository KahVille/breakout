#include "Paddle.h"

Paddle::Paddle() : GameObject()
{
    this->width = 96.0;
    this->height = 24.0;
    //std::cout << "Paddle Created.." << '\n';
    //GameObject::setPosition(sf::Vector2f(300.0f,300.0f));
    GameObject::setSprite("../textures/BasicArkanoidPack.png");
    GameObject::sprite.setTextureRect(sf::IntRect(184, 112, this->width, this->height));

}
Paddle::~Paddle()
{
    this->width = 0.0;
    this->height = 0.0;
    //std::cout << "Paddle Destroyed.." << '\n';
}

void Paddle::update(float mousex)
{
    mousex = (mousex - (this->width/2));
    setPosition(sf::Vector2f(mousex,GameObject::getPosition().y));
}
sf::Sprite Paddle::getSkin()
{
  return GameObject::sprite;
}

float Paddle::getWidth() {return this->width; }
float Paddle::getHeight() {return this->height; }

void Paddle::setWidth(float width) {this->width = width; }
void Paddle::setHeight(float height) {this->height = height; }
