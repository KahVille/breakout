#include "Wall.h"

//constructor
Wall::Wall(float width, float height) : GameObject()
{
	//make this size of renderer window
	this->width = width;
	this->height = height;
	this->thickness = this->height;
	//std::cout << "Wall Created.." << '\n';
	//sprite.scale(10.0, 0.5);
	//sprite.setColor(sf::Color(255, 255, 255, 128)); // half transparent
    GameObject::setSprite("../textures/wallsprite.png");
    GameObject::sprite.setTextureRect(sf::IntRect(10, 10, this->width, this->height));
	//GameObject::sprite.setRepeat(true);
	


}

//deconstructor
Wall::~Wall() {
	this->width = 0.0;
	this->height = 0.0;
	//std::cout << "Wall Destroyed.." << '\n';
}

//getters
float Wall::getWidth()
{
	return this->width;
}
float Wall::getHeight()
{
	return this->height;
}
float Wall::getThickness()
{
	return this->thickness;
}

//setters
void Wall::setWidth(float width)
{
	this->width = width;
}
void Wall::setHeight(float height)
{
	this->height = height;
}
void Wall::setThickness(float thickness)
{
	this->thickness = thickness;
}

sf::Sprite Wall::getSkin()
{
	return GameObject::sprite;
}


void Wall::update() {

}
