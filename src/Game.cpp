#include "Game.h"

  Game::Game(sf::RenderWindow* window) {

    pgameArea = new GameArea(window->getSize().x, window->getSize().y);
    pball = new Ball(15.0f);

    ppaddle = new Paddle();
    ppaddle->setPosition(sf::Vector2f(300.0f,pgameArea->GetHeight()-30)); //position of inherited GameObject
    //walls
    topwall = new Wall(pgameArea->GetWidth()- 2*pgameArea->getWallThickness(),  pgameArea->getWallThickness());
    topwall->setPosition(sf::Vector2f(0.0f, 0.0f)); //position of inherited GameObject

    rightwall = new Wall( pgameArea->getWallThickness(), pgameArea->GetHeight());
    rightwall->setPosition(sf::Vector2f(pgameArea->GetWidth() - pgameArea->getWallThickness(), 0.0));

    leftwall = new Wall(pgameArea->getWallThickness(), pgameArea->GetHeight());
    leftwall->setPosition(sf::Vector2f(0.0f, 0.0f));

    //level
    level = new Level();

    //Audio
    bounceSound = new Audio("../audio/bounce.ogg");
    deathSound = new Audio("../audio/destroy.ogg");
    
    if (!mainmusic.openFromFile("../audio/song.ogg"))
    {
        //std::cout << "music could not be loaded" << '\n';
    }
    else
    { mainmusic.setLoop(true);
      mainmusic.play();
    }
    
    renderer = new Renderer();

    gameState = Playing;
    //std::cout << "new Game Created! " << '\n';
  }

  Game::~Game() {

    //delete all the things
    delete renderer;
    delete deathSound;
    delete bounceSound;
    delete topwall;
    delete leftwall;
    delete rightwall;
    delete ppaddle;
    delete pball;
    delete pgameArea;
    delete level;
    
  }

  void Game::reset() {
    gameState = Playing;

    delete pball;
    pball = new Ball(15.0f);

    delete level;
    level = new Level();
    createLevel(nextlevel);

   mainmusic.setLoop(true); // make it loop
   mainmusic.play();
  }

  void Game::Update(float paddlex) {
  //  //std::cout << Game::GetGameState() << std::endl;
  
  this->CheckCollisions();
  pball->update();
  level->updateTiles();
  ppaddle->update(paddlex);
 
   
   //limit paddle position in the game area
   //right wall check for paddle position
   if((ppaddle->getPosition().x + ppaddle->getWidth()) >= rightwall->getPosition().x) {
       ppaddle->setPosition(sf::Vector2f((rightwall->getPosition().x-ppaddle->getWidth()), ppaddle->getPosition().y));
   }
   //left wall check for paddle position
   if(ppaddle->getPosition().x<= leftwall->getPosition().x) {
      ppaddle->setPosition(sf::Vector2f((leftwall->getPosition().x), ppaddle->getPosition().y));
   }

  // Check if the game is over and no success; failed to complete level
    if (pball->getPosition().y > pgameArea->GetHeight()){
        deathSound->playSound();
        mainmusic.stop();
        this->gameState = Ended;
        if(!level->iscomplete()) --currentlevel;
    }

    //level is complete advance to next level
     if(level->iscomplete()) {
     advanceLevel();
     reset();
     }

}

void Game::Render(sf::RenderWindow* pWindow) {
    // Clear screen
    pWindow->clear();

     //draw walls
    pWindow->draw(leftwall->getSkin());
    pWindow->draw(topwall->getSkin());
    pWindow->draw(rightwall->getSkin());
    
    //draw paddle and ball textures
    pWindow->draw(ppaddle->getSkin());
    pWindow->draw(pball->getSkin());

    // draw bricks
    for(Tile* n : level->getTilemap())
        pWindow->draw(n->getSprite());

    // Update the window
    pWindow->display();

}

void Game::CheckCollisions() {
    wallsCheck();
    tileCheck();
    paddleCheck();
}

void Game::paddleCheck() {
      // paddle check
    if (pball->getCenterPoint() > ppaddle->getPosition().x &&
    pball->getCenterPoint() <= ppaddle->getPosition().x + ppaddle->getWidth())
    {
        if (pball->getPosition().y+ (2*pball->getRadius()) >= ppaddle->getPosition().y &&
          pball->getVelocity().y > 0.0f)
        {
        bounceSound->playSound();
        //calculate how much ball position differs from paddle's middle point'
        float ballmiddle = pball->getCenterPoint();
        float paddlemiddle = (ppaddle->getPosition().x + ppaddle->getWidth()/2) ;
        float distance = paddlemiddle - ballmiddle;
       // std::cout << "distance from middle "<<"m: " << distance << "b: " << ballmiddle << " p: "<< paddlemiddle <<'\n';
         //pball->bounceHorizontally();
        pball->bounceOff(sf::Vector2f( -distance, -80));

        }
    }
}

void Game::tileCheck() 
{
     
  // tile check
  for(Tile* tile : level->getTilemap()) 
  {
     if (pball->getCenterPoint() > tile->getPosition().x &&
    pball->getCenterPoint() <= tile->getPosition().x + ppaddle->getWidth())
    {
      //tiilen alareuna kun tullaan alhaalta ylös
        if (pball->getPosition().y <= tile->getPosition().y && pball->getVelocity().y < 0.0f)
        {
          pball->bounceHorizontally();
          bounceSound->playSound();
          tile->Damaged();
          break;
        }
        //tiilen yläreuna kun ylhäältä alas
        if (pball->getPosition().y+ (2*pball->getRadius()) >= tile->getPosition().y &&
            pball->getVelocity().y > 0.0f)
        {
                 pball->bounceHorizontally();
                 bounceSound->playSound();
                 tile->Damaged();
                 break;
        }
    }
  }

}

void Game::wallsCheck() {
      // right wall check
    if(pball->getCenterPoint()>= rightwall->getPosition().x)
    {
        pball->bounceVertically();
        bounceSound->playSound();
    }

    // left wall check
    if (pball->getCenterPoint() <= leftwall->getPosition().x + pgameArea->getWallThickness() )
    {
        pball->bounceVertically();
        bounceSound->playSound();
    }

    // top wall check
        if (pball->getPosition().y <= topwall->getPosition().y && pball->getVelocity().y < 0.0f)
    {
        pball->bounceHorizontally();
        bounceSound->playSound();
    }
}


void Game::HandleInput()
{

}

void Game::advanceLevel()
{
    //level looping do when level is completed
    nextlevel = currentlevel +1;
    if(currentlevel == 3) {
      currentlevel = 0;
      nextlevel = currentlevel +1;
    }

}

void Game::ReadLevels() {
      infilestream.open("../levels/levels.txt");

      while( std::getline(infilestream, line))
      {
          for (unsigned i=0; i<line.length(); ++i)
                {
                  if(line.at(i) == '1') {
                    numberofbriks++;
                    levelsdata.push_back(1);
                  }
                         if(line.at(i) == '2') {
                    numberofbriks++;
                    levelsdata.push_back(2);
                  }
                    if(line.at(i) == '3') {
                    numberofbriks++;
                    levelsdata.push_back(3);
                  }
                  else if(line.at(i) == '0') {
                    numberofemptys++;
                    levelsdata.push_back(0);
                  }
                  else if(line.at(i) == '-') {
                  levelsdata.insert( levelsdata.end(), {numberofbriks, numberofemptys} );
                  numberofbriks = 0;
                  numberofemptys = 0;
                }
          }
    }
      infilestream.close();
}

void Game::createLevel(int level) {
  int levelselector = level; //level to be created
  std::vector<int> leveldata;
 
    switch (levelselector) {

      //level 1
      case 1:
      for (auto it = levelsdata.begin(); it != levelsdata.begin() + 9; ++it)
      {
        if(*it == 0) leveldata.push_back(0);
        if(*it == 1) leveldata.push_back(1);
        if(*it == 2) leveldata.push_back(2);
        if(*it == 3) leveldata.push_back(3);
        

      }
      currentlevel = levelselector;
      break;

      //level 2
      case 2:
      for (auto it = levelsdata.begin()+11; it != levelsdata.end() - 13; ++it)
      {
        if(*it == 0) leveldata.push_back(0);
        if(*it == 1) leveldata.push_back(1);
        if(*it == 2) leveldata.push_back(2);
        if(*it == 3) leveldata.push_back(3);
      }
      currentlevel = levelselector;
      break;

      //level 3
      case 3:
      for (auto it = levelsdata.end()-11; it != levelsdata.end()-2; ++it)
      {
        if(*it == 0) leveldata.push_back(0);
        if(*it == 1) leveldata.push_back(1);
        if(*it == 2) leveldata.push_back(2);
        if(*it == 3) leveldata.push_back(3);
      }
      currentlevel = levelselector;
      break;

      //no suitable value break by default
      default:
      break;
    }
    this->level->addTiles(leveldata);
    leveldata.clear();
}
