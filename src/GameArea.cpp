#include "GameArea.h"

GameArea::GameArea()
{
    this->width = 400.0;
    this->height = 400.0;
    wallthickness = 25.0;

    //std::cout << "new GameArea Created... " << std::endl;

}
GameArea::GameArea(int width, int height)
{
    this->width = width;
    this->height = height;
    wallthickness = 25.0;
    //std::cout << "Game Area created" << this->width << this->height << std::endl;
}

GameArea::~GameArea()
{
    width = 0.0;
    height = 0.0;
    wallthickness = 0.0;
    //std::cout << "GameArea Destroyed... " << std::endl;
}

float GameArea::GetWidth() {return this->width; }

void GameArea::SetWidth(float width) {this->width = width; }

float GameArea::GetHeight() {return this->height; }

void GameArea::SetHeight(float width) {this->width = height; }

float GameArea::getWallThickness() {return this->wallthickness; }

void GameArea::setWallThickness(float thickness) {this->wallthickness = thickness; }
