#include "Ball.h"

Ball::Ball(float radius) : GameObject() {
    this->radius = radius;
    this->velocity = sf::Vector2f(3.0,-3.0);
    GameObject::setPosition(sf::Vector2f(300.0f,500.0f));
    GameObject::setSprite("../textures/BasicArkanoidPack.png");
    GameObject::sprite.setTextureRect(sf::IntRect(0, 74, radius*2, radius*2));

    
}

Ball::~Ball() { //std::cout << "Ball Destroyed... " << std::endl;
}

void Ball::update()
{
    GameObject::setPosition(GameObject::getPosition() + getVelocity());
    ////std::cout << "pos" << " x "<<GameObject::getPosition().x << " y " << GameObject::getPosition().y << '\n';
    //calculate centerpoint

}
float Ball::getCenterPoint() {
    this->centerpoint = GameObject::getPosition().x + this->radius;
   // std::cout << this->centerpoint << '\n';
    return this->centerpoint;
}

void Ball::setVelocity(sf::Vector2f vel) { this->velocity = vel; }

float Ball::getRadius() {return this->radius; }

void Ball::setRadius(float radius) {this->radius = radius;}

sf::Vector2f Ball::getVelocity() { return this->velocity; }

sf::Sprite Ball::getSkin() {return GameObject::sprite; }

void Ball::bounceOff(sf::Vector2f n)
{
    sf::Vector2f r;

    double length_n = (n.x*n.x+n.y*n.y);
    double dotprod = (this->velocity.x*n.x + this->velocity.y*n.y);

    r.x = this->velocity.x - 2.0f*(dotprod/length_n)*n.x;
    r.y = this->velocity.y - 2.0f*(dotprod/length_n)*n.y;

    
    //std::cout << "vel: " << velocity.x << "," << velocity.y << std::endl;
    //std::cout << "normal: " << n.x << "," << n.y << std::endl;
    //std::cout << "reflection: " << r.x << "," << r.y << std::endl;
    

    this->velocity = r;

}

void Ball::bounceHorizontally()
{
    setVelocity(sf::Vector2f(getVelocity().x,(getVelocity().y) *(-1) ));
}

void Ball::bounceVertically() {
    setVelocity(sf::Vector2f(getVelocity().x*(-1) ,getVelocity().y));
}