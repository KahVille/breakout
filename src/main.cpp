#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "Game.h"

int main()
{

  sf::RenderWindow window(sf::VideoMode(1280, 720), "Breakout sfml");
  window.setFramerateLimit(60);
  bool isFullscreen = false;
  Game* game = new Game(&window);
  float mousex = 300;
  game->ReadLevels();
  game->createLevel(1);

    while (window.isOpen())
  {
      sf::Event event;
      while (window.pollEvent(event))
      {
          if (event.type == sf::Event::Closed) {
            window.close();
            delete game;
            return 0;
          }
          //check mouse position
          if (event.type == sf::Event::MouseMoved){
              mousex = event.mouseMove.x;
          }
          if (event.type == sf::Event::KeyPressed)
          {
            if (event.key.code == sf::Keyboard::Escape)
            {
              window.close();
              delete game;
              return 0;
            }
          }
        // If game is over and mouse button pressed...
          if (event.type == sf::Event::MouseButtonPressed && game->gameState == 1)
          {

             // if game is ended gnu 4.9 removed Game::states! enum
              //... reset ball and game over state
              game->reset();

          }
          //more events
          if(event.type == sf::Event::KeyReleased)
          {
              if (event.key.code == sf::Keyboard::F)
              {
                  //std::cout << isFullscreen << '\n';
                  if(isFullscreen == true)
                  {
                      window.close();
                      window.create(sf::VideoMode(1280, 720), "Breakout sfml");
                      window.setFramerateLimit(60);
                      isFullscreen = false;
                  }
                  else
                  {
                    // Display the list of all the video modes available for fullscreen
                    std::vector<sf::VideoMode> modes = sf::VideoMode::getFullscreenModes();
                    for (std::size_t i = 0; i < modes.size(); ++i)
                    {
                        sf::VideoMode mode = modes[i];
                        std::cout << "Mode #" << i << ": "
                                  << mode.width << "x" << mode.height << " - "
                                  << mode.bitsPerPixel << " bpp" << std::endl;
                    }

                  }
              }
            }

      }
      //if game is playing or paused
      if(!(game->gameState == 1)) { //if game is not ended enum
        game->Update(mousex);
      }

        //render the game window after all game updates
        game->Render(&window);
    }

    return 0;
  }
